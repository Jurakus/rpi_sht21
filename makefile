#*****************************************************************************#
#
#         _               _ _
#        | |             //| |
#        | |_   _ _ __ __ _| | ___   _ ___
#    _   | | | | | '__/ _` | |/ / | | / __|
#   | |__| | |_| | | | (_| |   <| |_| \__ \
#    \____/ \__,_|_|  \__,_|_|\_\\__,_|___/
#
#              www.jurakus.cz
#
# Filename:    Makefile
# Version:     0.0.1
# Description: Makefile
# Project:     Rpi-SHT21
# Author:      Jurákus, jurakus@jurakus.cz
# Create Date: 04.01.2014
# Modify Date: 04.01.2014
# Target:      Raspberry Pi
# Tabsize:     4
# Copyright:   (c) 2009 by Jurákus
# License:     GNU GPL v2
#
#*****************************************************************************#

# Compiler
CC = gcc

#CFLAGS  = -mtune=arm1176jzf-s -mfpu=vfp -mfloat-abi=hard -marmv6z -Wall
#CFLAGS  = -mtune=arm1176jzf-s -mfpu=vfp -mfloat-abi=hard -marm -O3 -Wall
CFLAGS = -march=armv6j -mfpu=vfp -mfloat-abi=hard -O2 -Wall
#CFLAGS = -O2 -Wall
LD = ld
LDFLAGS =

INCLUDE_DIR = ./src/

TARGET = rpi_sht21
SRC = $(INCLUDE_DIR)main.c 
SRC += $(INCLUDE_DIR)bcm2835.c
SRC += $(INCLUDE_DIR)i2c.c
SRC += $(INCLUDE_DIR)sht21.c
SRC += $(INCLUDE_DIR)rpi.c

OBJ = $(SRC:.c=.o)

all: $(SRC) $(TARGET)

$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $(TARGET)

%.o : %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -rf $(SRC:.c=.o)
	rm -rf $(TARGET)

