/******************************************************************************
//
//         _               _ _
//        | |             //| |
//        | |_   _ _ __ __ _| | ___   _ ___
//    _   | | | | | '__/ _` | |/ / | | / __|
//   | |__| | |_| | | | (_| |   <| |_| \__ \
//    \____/ \__,_|_|  \__,_|_|\_\\__,_|___/
//
//              www.jurakus.cz
//
// Filename:    rpi.c
// Version:     0.0.1
// Description: Raspberry Pi Library
// Project:     Rpi-SHT21
// Author:      Jur�kus, jurakus@jurakus.cz
// Create Date: 04.01.2014
// Modify Date: 04.01.2014
// Target:      Raspberry Pi
// Tabsize:     4
// Copyright:   (c) 2014 by Jur�kus
// License:     GNU GPL v2
//
// Open Source Licensing
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
******************************************************************************/

/// Includes *****************************************************************/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "std_c.h"
#include "rpi.h"


/// Preprocessing directives (#define) ***************************************/

/// Type definitions (typedef) ***********************************************/

/// Global constants *********************************************************/

/// Global variables *********************************************************/

/// Local constants  *********************************************************/

/// Local variables **********************************************************/

/// Global functions *********************************************************/

/**--------------------------------------------------------------------------**
 * Name:        GetRaspberryHwRevision
 * Function:    Check wich Hardware is used:
                http://www.raspberrypi.org/archives/1929
 * Parameter:
 * Return:      0 = no info , 1 = HW Rev.1, 2 = HW Rev.2
*----------------------------------------------------------------------------*/
int GetRaspberryHwRevision(void)
{
	FILE *fp;
	char line[32];
	char s[32];
	int i;

	fp = fopen("/proc/cpuinfo", "r");		// open as file
	if(fp != NULL)
	{
		while(fgets(line,32,fp))			// get line
		{
			sscanf(line,"%s : %x",(char*)&s,&i);	// parse for key and value
			//printf("[%s] [%i]\r\n",s,i);
			if(strcmp(s,"Revision") == 0)		// check for "Revision"
			{
				//printf("Found: %s=%i\r\n",s,i);
				if(i < 4)  return 1;
				else		return 2;
			}
		}
	}
	else
	{
		//printf("cpuinfo not available.\r\n");
		return 0;
	}
	//printf("no revision info available.\r\n");
	return 0;
}

/**--------------------------------------------------------------------------**
 * Name:        DelayMs
 * Function:    Delay for Milliscond

 * Parameter:   Number of Millisconds
 * Return:      -
*----------------------------------------------------------------------------*/
void DelayMs(uint32 ms) {
  struct timespec t, dummy ;

  t.tv_sec  = (time_t)(ms / 1000) ;
  t.tv_nsec = (long)(ms % 1000) * 1000000 ;
  nanosleep (&t, &dummy);
}

/**--------------------------------------------------------------------------**
 * Name:        DelayUs
 * Function:    Delay for Microscond

 * Parameter:   Number of Microsconds
 * Return:      -
*----------------------------------------------------------------------------*/
void DelayUs(uint32 us) {
  struct timespec t, dummy ;

  t.tv_sec  = 0 ;
  t.tv_nsec = (long)(us * 1000) ;
  nanosleep (&t, &dummy);
}

/// Local functions **********************************************************/
