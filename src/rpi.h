/******************************************************************************
//
//         _               _ _
//        | |             //| |
//        | |_   _ _ __ __ _| | ___   _ ___
//    _   | | | | | '__/ _` | |/ / | | / __|
//   | |__| | |_| | | | (_| |   <| |_| \__ \
//    \____/ \__,_|_|  \__,_|_|\_\\__,_|___/
//
//              www.jurakus.cz
//
// Filename:    rpi.h
// Version:     0.0.1
// Description: Raspberry Pi Library
// Project:     Rpi-SHT21
// Author:      Jurákus, jurakus@jurakus.cz
// Create Date: 04.01.2014
// Modify Date: 04.01.2014
// Target:      Raspberry Pi
// Tabsize:     4
// Copyright:   (c) 2009 by Jurákus
// License:     GNU GPL v2
//
******************************************************************************/

#ifndef RASPI_H
#define RASPI_H

/// Includes *****************************************************************/

#include "std_c.h"

/// Preprocessing directives (#define) ***************************************/

/// Type definitions (typedef) ***********************************************/

/// Global constants (extern) ************************************************/

/// Global variables (extern) ************************************************/

/// Global functions *********************************************************/

int GetRaspberryHwRevision(void);
void DelayMs(uint32 ms);
void DelayUs(uint32 us);

#endif // RPI_H
