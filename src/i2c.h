/******************************************************************************
//
//         _               _ _
//        | |             //| |
//        | |_   _ _ __ __ _| | ___   _ ___
//    _   | | | | | '__/ _` | |/ / | | / __|
//   | |__| | |_| | | | (_| |   <| |_| \__ \
//    \____/ \__,_|_|  \__,_|_|\_\\__,_|___/
//
//              www.jurakus.cz
//
// Filename:    i2c.h
// Version:     0.0.1
// Description: Library for I2C on Raspberry Pi
// Project:     Rpi-SHT21
// Author:      Jurákus, jurakus@jurakus.cz
// Create Date: 04.01.2014
// Modify Date: 04.01.2014
// Target:      Raspberry Pi
// Tabsize:     4
// Copyright:   (c) 2009 by Jurákus
// License:     GNU GPL v2
//
******************************************************************************/


#ifndef I2C_H
#define I2C_H

/// Includes *****************************************************************/

#include "std_c.h"

/// Preprocessing directives (#define) ***************************************/

/// Type definitions (typedef) ***********************************************/

/// Global constants (extern) ************************************************/

/// Global variables (extern) ************************************************/

/// Global functions *********************************************************/

void  SI2C_SetPort(uint8 Scl,uint8 Sda);
void  SI2C_Start(void);
void  SI2C_Stop(void);
uint8 SI2C_SendByte(uint8 Data);
uint8 SI2C_ReadByte(uint8 Ack);
void  SI2C_SetSclState(uint8 State);
uint8 SI2C_GetSclState(void);

#endif // I2C_H
