/******************************************************************************
//
//         _               _ _
//        | |             //| |
//        | |_   _ _ __ __ _| | ___   _ ___
//    _   | | | | | '__/ _` | |/ / | | / __|
//   | |__| | |_| | | | (_| |   <| |_| \__ \
//    \____/ \__,_|_|  \__,_|_|\_\\__,_|___/
//
//              www.jurakus.cz
//
// Filename:    main.c
// Version:     0.0.1
// Description:
// Project:     Rpi-SHT21
// Author:      Jur�kus, jurakus@jurakus.cz
// Create Date: 04.01.2014
// Modify Date: 04.01.2014
// Target:      Raspberry Pi
// Tabsize:     4
// Copyright:   (c) 2009 by Jur�kus
// License:     GNU GPL v2
//
// Open Source Licensing
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
******************************************************************************/

/// Includes *****************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "bcm2835.h"
#include "rpi.h"
#include "i2c.h"
#include "sht21.h"
#include "time.h"

/// Preprocessing directives (#define) ***************************************/

/// Preprocessing directives (#define) ***************************************/

/// Type definitions (typedef) ***********************************************/

/// Global constants *********************************************************/

/// Global variables *********************************************************/

/// Local constants  *********************************************************/

/// Local variables **********************************************************/

/**--------------------------------------------------------------------------**
 * Name:        main
 * Function:    Main Function

 * Parameter:
 * Return:      Error Code
*----------------------------------------------------------------------------*/
int main(int argc, char **argv) {

	int32	Temperature;
	int32	Humidity;
	uint8	SHT21_Error;
	float 	Temp, Humid;
	FILE	*fp;
	time_t TimeCounter;
	time_t TimeCounterLocal;
	struct tm * Time;
	int 	HwRev;

	printf("Rpi-SHT21 V0.0.1 by Jurakus (www.jurakus.cz) [" __DATE__ " " __TIME__"]\n");

	if (!bcm2835_init())
	{
		printf("bcm2835_init() failed!\r\n");
		return 1;
	}

	HwRev = GetRaspberryHwRevision();
	printf("RaspberryHwRevision=%i\r\n",HwRev);

	if(HwRev < 2) 	SI2C_SetPort(1,0);	 // Hardware Revision 1.0
    else		    SI2C_SetPort(3,2);   // Hardware Revision 2.0

	while (1) {
		time ( &TimeCounter );

		if((TimeCounter % 10) == 0)	{	// alle 10 sec messen
			Time = localtime (&TimeCounter);   // wichtig f�r Timezone und Sommerzeit
			TimeCounterLocal = TimeCounter - timezone;
			if(Time->tm_isdst) TimeCounterLocal += 3600;	// Sommerzeit

			printf("%u ",(unsigned int)TimeCounterLocal);
			printf("%02d.%02d.%d %02d:%02d:%02d ",Time->tm_mday,Time->tm_mon+1,Time->tm_year+1900,Time->tm_hour, Time->tm_min, Time->tm_sec);


			SHT21_Error = SHT21_Read(&Temperature,&Humidity);
			Temp = Temperature;
			Humid = Humidity;
			Temp /= 1000;
			Humid /= 1000;

			printf("%.3f\t%.3f\r\n", Temp, Humid);

			if(SHT21_Error) printf("ERROR=0x%X\r\n",SHT21_Error);

		}
		sleep(1);
    }
	return(0);
}
