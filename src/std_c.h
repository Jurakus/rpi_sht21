/******************************************************************************
//
//         _               _ _
//        | |             //| |
//        | |_   _ _ __ __ _| | ___   _ ___
//    _   | | | | | '__/ _` | |/ / | | / __|
//   | |__| | |_| | | | (_| |   <| |_| \__ \
//    \____/ \__,_|_|  \__,_|_|\_\\__,_|___/
//
//              www.jurakus.cz
//
// Filename:    std_c.h
// Version:     0.0.1
// Description: Macros to make source compile on both ANSI C and K&R C
// Project:     Rpi-SHT21
// Author:      Jurákus, jurakus@jurakus.cz
// Create Date: 04.01.2014
// Modify Date: 04.01.2014
// Target:      Raspberry Pi
// Tabsize:     4
// Copyright:   (c) 2009 by Jurákus
// License:     GNU GPL v2
//
******************************************************************************/

#ifndef STD_C_H
#define STD_C_H

/// Includes *****************************************************************/

/// Preprocessing directives (#define) ***************************************/


// Boolean values

#define TRUE                1
#define FALSE               0

#define true                1
#define false               0

/// Type definitions (typedef) ***********************************************/

/// Standard types

typedef unsigned char     uint8;
typedef unsigned short    uint16;
typedef unsigned long     uint32;

typedef unsigned long     ulong;

typedef unsigned char     bool;

typedef signed char       int8;
typedef signed short      int16;
typedef signed long       int32;

/// Global constants (extern) ************************************************/

/// Global variables (extern) ************************************************/

/// Global functions *********************************************************/

#endif //STD_C_H
